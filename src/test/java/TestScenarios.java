import com.google.gson.Gson;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import models.RegisterUser;
import models.user.Datum;
import models.user.User;
import org.junit.Test;


import java.util.List;

import static io.restassured.RestAssured.given;

public class TestScenarios extends BaseApi {

    @Test
    public void successfulRegistration() {


        String payload = "{\n" +
                "    \"email\": \"michael.lawson@reqres.in\",\n" +
                "    \"password\": \"white\"\n" +
                "}";

        Response response = given()
                .contentType("application/json")
                .body(payload)
                .accept(ContentType.JSON)
                .log().all()
                .when()
                .request("POST", "/api/register").prettyPeek()
                .then()
                .assertThat().statusCode(200).extract().response();

        //LOGGER.info(response.asString());

        //String id = response.jsonPath().getString("id");
        //String token = response.jsonPath().getString("token");

        //LOGGER.info("ID : "+ id + " TOKEN : "+ token);

        //Another approach
        Gson gson = new Gson();
        RegisterUser registerUser = gson.fromJson(response.body().prettyPrint(), RegisterUser.class);
        LOGGER.info("ID : " + registerUser.getId() + " TOKEN : " + registerUser.getToken());


    }

    @Test
    public void unsuccessfulRegistration() {


        String payload = "{\n" +
                "    \"email\": \"eve.holt@reqres.in\"\n" +
                "    \n" +
                "}";

        ValidatableResponse response = given()
                .contentType("application/json")
                .body(payload)
                .accept(ContentType.JSON)
                .log().all()
                .when()
                .request("POST", "/api/register").prettyPeek()
                .then()
                .assertThat().statusCode(400);

    }

    @Test
    public void getUserList() {

        Response response = given()
                .contentType("application/json")
                .accept(ContentType.JSON)
                .log().all()
                .when()
                .request("GET", "/api/users").prettyPeek()
                .then()
                .assertThat().statusCode(200).extract().response();

        //LOGGER.info(response.asPrettyString());


        Gson gson = new Gson();
        User registeredUsers = gson.fromJson(response.body().prettyPrint(), User.class);


        List<Datum> datums = registeredUsers.getData();

        for (Datum datum : datums)
            LOGGER.info("FIRST NAME : " + datum.getFirstName() + " LAST NAME : " + datum.getLastName());
    }

}
